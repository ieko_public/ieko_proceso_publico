# IEKO PROCESO PUBLICO

**VERSIÓN COMPARTIDA DEL "PROCESO DE DESARROLLO DE SOFTWARE IEKO"  Versión 1.1**
**IEKO 2021**
Compartido bajo [licencia de CopyLeft LGPL](https://gitlab.com/ieko_public/ieko_proceso_publico/-/blob/master/LICENSE).


**Este proceso es la base de todos nuestros proyectos. Nuestro objetivo es que cada tarea que guía el funcionamiento de nuestra maquinaria se encuentre incluida en él.**

**--INTRODUCCIÓN--**


Deseamos profundamente que este proceso te sea de utilidad, lo puedas compartir con tu equipo y colegas, y que se sumen a mejorarlo continuamente. 


----------

**¡Te damos la bienvenida a IEKO!** 

Somos desarrolladores de software especializados en el mercado financiero.  

Proponemos un nuevo paradigma de **Software Sustentable**.  

Mediante un esquema de repositorios GIT, **liberamos conocimiento** y subproductos para que sean compartidos, reutilizados y enriquecidos por los actores del ecosistema. Los aportes recibidos como feedback son evaluados y, si se consideran de valor, incorporados. 

El proyecto IEKO está en desarrollo: **te invitamos a participar**, hay mucho por descubrir. 

Compartimos bajo una licencia de CopyLeft LGPL. Cuando uses este proyecto o lo compartas, **sé responsable con el ecosistema** ;) 

En el footer encontrás **más información** sobre nosotros y cómo ponerte en contacto.  

¡Gracias por tu visita y tu feedback! 

**Team IEKO**  

----------

**--CÓMO EXPORTAR EL PROCESO EN TU PROYECTO--**

Puedes comenzar a utilizar y configurar un proyecto con este proceso de la siguiente manera: 

1) Ve a la sección settings/advanced y exporta el proyecto de esta manera: https://docs.gitlab.com/ee/user/project/settings/import_export.html
2) Crea tu repositorio Gitlab e importa nuestro proceso de esta manera: https://docs.gitlab.com/ee/user/project/settings/import_export.html

3) Lee la sección --CÓMO USAR EL PROCESO DE DESARROLLO IEKO--
4) comienza con la tarea "00.1 Asignar Roles Iniciales" del Board "00 - Configuración Inicial" . 


**--CÓMO USAR EL PROCESO DE DESARROLLO IEKO--**

_Cómo utilizar el proceso:_

A continuación, encontrarás un índice general de las secciones del proceso.

**El objetivo es ir completando las tareas iniciales e ir siguiendo los flujos de trabajos hasta llegar al tablero de desarrollo.**


Hemos dejado algunas tareas o indicaciones sin completar, para que puedas aportar tu creatividad y elijas cómo resolver ciertos hitos. Nos gustaría mucho conocer cómo resolviste cada parte, tus aportes son bienvenidos.


**ÍNDICE:**

_Para cada nuevo proyecto IEKO, el Devop deberá:_
- exportar este proyecto completo
- crear el nuevo proyecto dentro del grupo IEKO  -
- importar el proyecto exportado
- cambiar en los Links de las tareas las URL a los documentos y tareas según el nombre del proyecto nuevo
- asignarse a sí mismo la tarea 00.01


_El proceso cuenta con 5 Boards_. Cada Board contiene las columnas por las que deben ir pasando las tareas a medida que vayan cambiando de estado. 

* **0)** Configuración Inicial
* **1)** Definición Inicial
* **2)** PreDesarrollo
* **3)** Desarrollo
* **4)** Devop


**Detalle de los Boards:**

**0) Configuración Inicial:** En este tablero se encuentra la tarea de configuración inicial de Gitlab. Para Trabajo de DevOps

**1) Definición Inicial:** En este tablero están todas las tareas que hacen a las definiciones iniciales del proyecto. Para Trabajo PO PM A UX

**2) PreDesarrollo:** Configuraciones iniciales y armado de Backlog. Para Trabajo PM A UX DEV

**3) Desarrollo:**  Este es el board principal y de uso diario del proceso, donde se puede apreciar el estado general del proyecto al momento a partir de las UserStories y la planificación de las tareas.

_Columnas en el Board de Desarollo:_

* 03 Backlog:       -- En este punto se detallan las issues y los tags correspondientes a la columna de Backlog
* 03 ForDev:        -- En este punto se detallan las issues y los tags correspondientes a la columna de ForDev
* 03 InProgress:    -- En este punto se detallan las issues y los tags correspondientes a la columna de InProgress
* 03 Done:          -- En este punto se detallan las issues y los tags correspondientes a la columna de Done
* 03 Code Review:   -- En este punto se detallan las issues y los tags correspondientes a la columna de Code Review

**03.1)** --En este punto se debe especificar cómo cada tarea va fluyendo de una columna a otra hasta completar su ciclo en el board que la contiene.

**03.2)** Circuito de Aprobación

-- En este comentario se debe especificar cómo es el circuito de tags y cambio de roles correspondientes a la Aprobación por parte del cliente de cada una de las UserStories.

**03.3)** Entregas: -- En este comentario se detalla cómo se deben generar las Entregas de código, la nomenclatura, el versionado, el flujo de tags y la asignación de roles.

**04)** Board de Devop: este board contiene las columnas correspondientes al circuito de Pasajes entre los 3 ambientes que utilizará el Devop.

_Las Columnas son:_
- 04 Pend. Pasaje Desarrollo
- 04 En Amb. Desarrollo
- 04 Pend. Pasaje Testing
- 04 En Amb. Testing
- 04 Pend. Pasaje Producción
- 04 En Amb. Producción

Con respecto a las Columnas proporcionadas por defecto por Gitlab, no utilizaremos la columna OPEN, ya que utilizaremos el concepto con más amplitud distribuido en más columnas custom que resulten más representativas del estado de la tarea en sí. Sí utilizaremos la columna Closed para poder medir el avance de los Sprints.

**CONFIGURACIÓN GIT:**

_- Ramas principales:_

**1)** Desarrollo local
**2)** Desarrollo - Apuntando a Repositorio Git
**3)** Testing - Apuntando a Repositorio Git
**4)** Producción - Apuntando a Repositorio Git


_- Configuración de Repositorio Webapp Genexus a Git:_

--En esta sección se especifica cómo se puede versionar el código generado por Genexus con GIT , qué cuestiones se deben tener en cuenta, la correlación entre Issue, rama y xpz.


**-- CÓMO INICIAR EL RECORRIDO--**

La configuración del Proceso de Desarrollo de Software IEKO comienza con esta tarea "[00.1 Asignar Roles Iniciales](https://gitlab.com/ieko_public/ieko_proceso_publico/-/issues/1)" del Board "00 - Configuración Inicial" .


**-- METODOLOGÍAS Y TECNOLOGÍAS INVESTIGADAS PARA LA GENERACION DEL PROCESO IEKO--**

**1)** [Agile project management with Scrum](https://www.pmi.org/learning/library/agile-project-management-scrum-6269#:~:text=Scrum%20is%20one%20of%20the,efficiently%2C%20and%20effectively%20to%20change.)

**2)** [Gitlab](https://gitlab.com/)

**3)** [How to use GitLab for Agile software development](https://about.gitlab.com/blog/2018/03/05/gitlab-for-agile-software-development/)


----------

**Somos IEKO**: 

Brindamos soluciones de software para **negocios Fintech**: blockchain y criptoactivos,\
consultoría, desarrollos a medida,trading algorítmico, onboarding digital, APIficacación e integración de fuentes de datos. 

Nuestro objetivo es construir software siendo responsables con el ecosistema tecnológico\
en el que existimos, contribuyendo a la inclusión financiera, haciendo un aporte concreto al\
desarrollo económico y social. 

**Conocé nuestro proyecto y contactanos en**: 

- ieko.io 
- [LinkedIn IEKO](https://www.linkedin.com/company/ieko-io//) 
- [Instagram IEKO](https://www.instagram.com/ieko.io//) 
- [Twitter IEKO](https://www.instagram.com/ieko.io/) 
- [Facebook IEKO](https://www.facebook.com/ieko.io) 
- contacto@ieko.io

**Desde Rosario, Argentina, para el mundo** 

**#NosGustaHacerSoftware**
**#HacemosSoftwareSustentable**
**#InclusiónFinanciera** 

----------
